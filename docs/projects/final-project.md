# Final project

## The reed lamp

My first idea that comes in mind are a natural beautiful floor lamp, that lights up the livingroom by the sofa. I dont know why but I have reed plants in my mind as an inspiration to the light structure and shape. Read more about the idea in ![Principles and practices](https://hellehauskov.gitlab.io/students_template_site/assignments/week01/)

The first sketch of my idea done in the first week looks like this...the concept needs some work! Just drawing the project by hand gave me some ideas and made me realise that there is a lot I still have to figure out.
![the first sketch of my final project](../images/week01/sketch-final-project.jpg)

# Week 2
I realised during this weeks process that maybe the foot stand shouldn't be a hemisphere but instead integrated in the post - maybe as casted concrete? Or epoxy? I realised that my project aren't that hard to draw in 3D yet. But I still have to figure out about the electronics placement and how to mount the footstand, these will definitely change my 3D drawings complexity.

Pictures from the fist drawings in vector and CAD:  
**Illustrator:**  
![Vector drawing made in Illustrator](../images/week02/lamp_vector.png)
**Onshape:**  
![Onshape drawing sheet](../images/week02/onshape-sheet.png)
