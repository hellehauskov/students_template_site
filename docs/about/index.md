# About me

![Photo of me](../images/week01/me.jpg)

My name is Helle Hauskov and I am from Denmark. I am Fab Lab manager at [FabLab Denmark](https://fablabdanmark.dk/wordpress/) in Næstved, a city based one hour south of Copenhagen. Visit us on Facebook [@FabLabDanmark] (https://da-dk.facebook.com/FabLabDanmark/) or [Instagram](https://www.instagram.com/fablabdanmark/). 

I have been a part of the Lab since 2013 and fulltime since 2015. I have more 10 years of experience as a graphic designer and that shows in my love for creatuvity, simplicity, shapes, aesthetics, great materials and the tactile feeling.
My favorite machine is still the lasercutter. I love that you can very fast make a prototype, and especially make beautiful engravings and expressions in wood.

The Fab Lab is a part of [Zealand Academy](https://zealand.com/) of Technologies and Business. We teach students at academy profession level to professional Bachelor Programmes level. We do a lot of innovation and rapid prototyping classes. It is a great way to make the students realize that they can invent and even become entreprenuers.


## Why I do this
I'm attending a new pilot project. A partial Fab Academy. I have been a part of the Fab Lab world for many years and have been eager to become a Fab Academy graduate. But as I am fulltime employed I didnt have the oppotunity to take a half year off to do the Fab Academy.
Our lab is a part of the [Nordic Fab Lab Network](https://nordicfablabs.org/) and it have provided me and the Lab valuable friendships and knowledge. Every year we gather for the Nordic Fab Lab Bootcamp in one of the nordic countries. It's a week of fun, geeking around, visiting local labs and teaching each other great skills. I have been on every bootcamp since 2017 in Lyngseidet, Norway, and it's the highlight of the year!
Anyway, we discussed if the Fab Academy could have another structure, so that people who aren't able to participate in a concentrated Fab Academy would still be able to get this valuable experience.
And now here I am! I am very excited to do this, learning how to make almost anything and still maintaing my job. I'm looking forward to upgrading my skills, as I'm not an engineer I have a lot to learn in every aspect of Fab Lab skills.



https://da-dk.facebook.com/FabLabDanmark/