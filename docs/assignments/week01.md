# 1. Principles and practices

This first week of Fab Academy I had to work on the documentation process and defining my final project. Students where in the first lesson introduced by Neil Gershenfeld to [the Fab Charter](https://fab.cba.mit.edu/about/charter/). I already knew it and I have actually made a big vinyl sticker for 'my Fab Lab, quoting the Fab Charter. If you don't know it please read the charter.

The assignment was to describe the first initial ideas of a final project, learn GIT, set it up and craeate my website. And of cource documenting it all. 

# The reed lamp
## Idea
My first idea that comes in mind are a natural beautiful floor lamp, that lights up the livingroom by the sofa. I dont know why but I have reed plants in my mind as an inspiration to the light structure and shape. It should be a descrete lamp.

I live in Denmark an we have dark and grey winters, and therefor also the great need of light in our houses. I would like the lamp to give an indirect light by shining against the wall. It should add a more natual light in the room.

## The design
Most floor lamps in Denmark takes up a lot of space. Their design are clumsy, undynamic and very locked. I think it would be interesting if the lamp could extend in some way. When folded, it is discreet and gives an indirect light. When needed it can extend in some way to reach above the sofa and coffee table. As it extends it reveals a downward LED strip and thereby produces more direct light 

Most lamps you just turn on and off and then it provides a certain amount of light, depending on the bulb that's installed. If my lamp could be ajusted in light level e.g. by touching/swiping the side of the lamp, it would be a much more versatile lamp. Maybe I can incoorperate some sort of light measuring sensor that could ajust the light during the day - especially the hours of sunrise/sunset. It could also be very exiting if the level og light could ajust, so the room have the same amount of light all the time. If you look at my first drawing (below) it also indicate that I have a small dream of making my lamp extend by pushing out an insert. How it will move I haven't figures out yet.

##Materials
The lamp is made of birch veneer to get that natural nordic feeling. The foot stand migth be castet to get the weight that the lamp need to balance. The on/off function would be nice to hide in some way, maybe under a thin layer of veneer.
The foot stand (or base) could also be made as a tall square, that keeps the electronics inside and a way to hide the counterweight.

##The skecth
The first sketch of my idea looks like this...the concept needs some work!
![the first sketch of my final project](../images/week01/sketch-final-project.jpg)

