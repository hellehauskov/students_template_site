# 4. Computer controlled cutting

This weeks assignment involves the lasercutter and vinylcutter.  

*The group assignment:*  
Characterize your lasercutter's focus, power, speed, rate, kerf, joint clearance and types.

*My individual assignment:*  
- Cut something on the vinylcutter.  
- Design, lasercut, and document a parametric construction kit, accounting for the lasercutter kerf, which can be assembled in multiple ways, and for extra credit include elements that aren't flat.

----
# Vinylcutting
I have used the Roland GX-24 vinylcutter for cutting the vinyl stickers needed for the doors to indicate studyrooms, it-department a.s.o. 

## Creating data in Illustrator  
First I used Illustrator for making the text with the corperate font that our academy use.  
![text in illustrator](../images/week03/1-illustrator-text.png)  

Then I outlined the text by right click on the text and choose "outline". Then the text are converted into vector lines.  
![outlining text](../images/week03/2-illustrator-outline.png){ width=40% }  

It can be tricky to remove the excess vinyl when you have a lot of details. To make the work easier I added a square around each word.  
![add quare](../images/week03/3-illustrator-add-square.png)  

Then I saved the file as an AI file on a USB stick. Remember to change the version to "Illustrator 8".  
![save settings](../images/week03/4-illustrator-AI-save-settings.png){ width=50% }  

## Setting up the vinylcutter  
First I chose the white vinyl roll and placed it on the backside of the vinylcutter.  
To insert the vinyl it is a good idea to push it in an angle. In this way the vinyl are less likely to get stuck in all the small openings that are inside the machine.  
Place the vinyl so the white marks on the machine are within the material. The white marks indicate where the two wheels can be placed. Push gently from the back on the wheels to adjust them. Always aim to have wheel placement as close to the edge of the material as possible. The wheels make sure that your material get evenly pushed forward and don't curl.  

When the material are placed in the right position, press down the handle on the left of the machine.  
![GX24 loading material](../images/week03/gx24-loading.jpg)  

Go to the controlpanel on the machine. The display will ask you if it's a roll or a piece of vinyl that you have inserted. Press `Enter` to choose the Roll option. Now it will automatically messure the width of the vinyl.  
![GX24 controlpanel](../images/week03/gx24-controlpanel.jpg)  

When that's done, make sure that the material are moved back as much as possible. Use the arrows to maneuver the head in to possition. Usually that's in the left buttom coner. Press and hold the `origin` button until `origin set` shows up on the display. Now you are ready to go!

## Setting up CutStudio  
Go to the computer assigned to the vinyl cutter and insert the USB stick and open the vinylcutters software "CutStudio".
Press the `Import` button on the top toolbar and choose your file. If you can't find the file, then check if the fileformat (button right) are set to AI, ESP fileformats.  
![CutStudio import](../images/week03/5-Cutstudio-import-object.png)  

When your object are loaded in to the program it should look something like this:  
![CutStudio imported object](../images/week03/6-Cutstudio-object.png)  
If needed you can move the objects by pressing them. You can also use the toolbar on the left to add squares, figures, text a.s.o.

To get the messurements of the vinyl roll, you go to File> Printing setup and then press ` Properties` . Then press the `Get from machine button`. CutStudio then gets the messurements that the vinylcutter have messured when you installed the vinyl in the machine. (You can also do this by pressing `cutting` in top toolbar.) Then your workarea adjusts to the new messurements. Always check that your work still are inside the white document (it represents the vinyl size).
![CutStudio get messurements](../images/week03/7-Cutstudio-messurements.png)  

The standard cutting setting on our machine are 20cm/mm, but in this case I have small letters and would like then machine to slow down so that it doesn't rip off the vinyl. I therefore change the "settings" on the next tab in the dialog box to half the speed by choosing `setting 1` and then change the cutting speed to 10 cm/sec.  
![CutStudio cutting speed](../images/week03/8-Cutstudio-cutting-speed.png)  

When the vinylcutter are done cutting, it just stops moving. Then you can lift the handle and remove the roll - or you can use the arrows on the controlpanel on the machine to move the knifehead to the right and then the arrow for up/down to move the material forward. Then take a knife and cut the piece off. Use the crack on the machine to guide the knife in a nice horizontal line.
![cutting the vinyl off the roll](../images/week03/vinyl-cutting.jpg)  

## Removing excess material
Theese tools makes life a lot easier when working with vinyl:  
![vinyl tools](../images/week03/9-vinyl-tools.jpg)  
A scissor, a knife, weeding tool, squeegees and a ruler.

I then start off by removing the excess vinyl from the backing.  
![vinyl removing vinyl](../images/week03/10-removing-excess-vinyl.jpg)  

Then I can easily cut each black square out with the knife.  
![vinyl cutting out squares](../images/week03/11-vinyl-squares2.jpg)  

Now I have each word seperated from the big sheet, and that makes it much easier to remove the excess vinyl around the letters. 
I use the weeding tool to peel off the excess material, so that it's only the letters I need that remains.  
![removing material around letters](../images/week03/12-removing-around-letters.jpg)  

When I'm done with peeling all the words, it looks like this:  
![vinyl done](../images/week03/13-vinyl-done.jpg)  

## Vinyl application
Apply masking tape on top of your letters, making sure that the tape covers the letters and then some. Use the squeegee-tool to carefully apply the masking tape, and secure that there is no bubbles or wrinkels. Finally, put a firm pressue on the whole area with the squeegee so that the masking tape attaches to the vinyl letters. Keep the backing on for now.  
![masking tape](../images/week03/14-masking-tape.jpg)  

Ensure that the surface you want to apply the vinyl to is clean and dry. Then carefully place the vinyl using the masking tape to adjust the placement on the surface. Be sure to measure and have it alligned so that you get a nice result.  
When it's all in place, carefully remove the backing bit by bit while using the squeegee tool with light pressure on the masking tape. This ensures that the vinyl attach to the surface without (that many) bubbles. Especially around the edges, it's important to check that it sticks to the surface.  

Lastly remove the masking tape by pulling down slowly and secure that the letters sticks to the surface and not the masking tape. If they do, try putting more pressure on the masking tape with the squeegee.  
![vinyl application](../images/week03/15-mount-on-door.JPG)  

![Me and one of the many doors](../images/week03/hero-vinyl-shot.jpg)  

----
# Lasercutting  

## The lasercutter setup in FabLab Denmark  
In the Fab Lab we have two Universal Lasercutters. They are 60W Co2 lasers, that operates in the inferred range, and have a working area of 450mm x 810mm x 229mm. It has a 2 inch 
  Each lasercutter are connected to a BOFA exhaust wich cleans the exhaust with a carbon and HEPA filter. 

The lasercutters in the lab each have a computer assigned with the software that controls the lasercutter - Universal Laser Systems Control Panel (UCP).  
![Lasercutters in FabLab Denmark](../images/week03/lasercuttersetup.jpg)  

Materials safe for lasercutting:
Acrylic (PMMA), Veneer (Interior), wood, cardboard, HDF, paper, most textiles, felt. There are more materials but theese are the most common. 

Materials NOT safe for lasercutting:  
PVC, PVB, Carbon, Teflon, materials that eeleases chlorine or hydrogen cyanide. There are more materials that are toxic when lasercut or engraved, be sure to talk to your Fab Lab manager or emplyee before using a material not listed here or on the Safe-list above. Always know what your material contains if its a material you bring to the Fab Lab.

Then there's materials that's not suitable for lasercutting or engraving - eg. food (it tastes bad).

## Operating the lasercutter (extra material)  

### Setting up the lasercutter  
First I turned on all the vents for the lasercutters by pressing the tree buttons on the wall next to the lasers. You will hear the small compressor underneath the laser start and the BOFA will light up.
![Lasercutter software UCP interface](../images/week03/ucp.png) *The UCP Interface*

The controlpanel in UCP have five red buttons. ![controlpanel UCP lasersoftware](../images/week03/ucp-controlpanel.png)  
1. `Zoom`: nice to be able to zoom on lines if you have to place the laserhead precise.  
2. `Focus view`: This clever button makes you control the laserhead and move it around the working area by pressing on the white workingarea on the screen.  
3. `Relocate view`: click on your drawing on the screen to move it around.  
4. `Duplicate view`: duplicates your drawing in the same working area. Good for businesscards and so on.  
5. `Estimae view"`: makes a time estimate of the job. Don't rely 100% on it.  

PLACING MATERIAL  
I then took some scrap HDF (High desity fiberboard) that fitted my project, and put it in the lasercutter. A good rule of thumb is to place the material in upper left corner - but it is not mandatory. It is important that the material are straight! If the material bends it could hit the laserhead when the laser is working - and that's critical!  
Now you have to set up the material thickness. Inside the lasercutter, on your left, you will find the `Focus Tool` (it´s a white and black plastic stick). Move the laserhead by selecting `Focus view` in the UCP and click in the area of your material. The laserhead then moves to that position. You will see a red LED pointer dot on your material - this is **not** the laserbeam.
Place it on top of your material and so it fits the laserhead in the laserhead as shown below.  
![Focus tool placement](../images/week03/focustool.jpg)  

If there it does not fit perfectly, adjust with the grey arrow buttons infront of the laser up or down (Z-axis). Press the buttons a few times, if you hold down the button it moves very fast.
When done place the Focus Tool back in place on the left inside the machine.  

PRINT FROM PDF  
Go to the lasers computer and open your pdf file and press `print` (check that `actual size` are selected so your drawing keeps the original size). Then go to the **UCP** and notice that your file are loaded in the software and you can see the graphics,like the picture below.  
![UCP job example](../images/week03/ucp-job.jpg)

SETTING UP the laser software  
Now you need to place your drawing precisely on the material. Use the `Focus view` again and click on the screen where you want the drawing upper left corner to be. Then the laserhead moves to that position. Click on `Relocate view`, check that the upper left corner on your object are blue. Then click on `Move to pointer` on the right of the screen. The the program moves the drawing to the excat position the red LED shows on the material. This comes in handy when you use scraps or materials that are not square - or if you want to place something in center.

Then you have to set the settings - press the red ```Settings```button. Here you adjust the settings for power - speed and PPI for each colour in the file, press ```Apply``` when done editing each colour.  
![UCP settings](../images/week03/ucp-settings.png)

Turn on the **BOFA** by pressing the power button on the BOFA. Then you press the green play button in the UCP, and the laser will start engraving and cutting. When the job is done, the play button will turn green again. Always wait 4-5 minutes before opening the lid so that the fumes are gone. When doing acrylic wait 10 minutes before opening.

## Making a parametric contruction kit
I have tried to make press fit before, but not that accurate as I need to do this week. The task is to make a parametric press fit construction kit that can be assembled in multiple ways, multiple times.  
First I had zero ideas on what to make. Then a tree came in mind, but I would like the tree to have a purpose. I then thought of a jewelry tree and how they are never big enough or doesn't meet all the needs for different types of jewelry. It's a girly thing I know, but none the less a source of annoyance.  


### Testing the lasercutter
I started by drawing a scale in Illustrator with different colors, so that I could test engraving and cutting settings. You can find the file in the last section on this page.
The laser have eigth colors it detects in the cutting file. We have a joint agreement in the Lab, to use the red color for cutting. By doing so we can easily understand peoples drawings and help if there is some issues.  

The colors are specific RGB colors as listed below:  
![color-mapping](../images/week03/colormapping.png)  
 
I chose to make my press fit contruction out of HDF as it's tolerance are very low and it doesn't get that affected by humidity and temperature (so it doesn't easily bend). I started out by making some slot and kerf tests.  

I selected the values for engraving and cutting from the standard settings we use in the lab for HDF and added values around these to see if we actually have the best settings. Based on earlier experience in the lab I used 500 PPI (points per inch). The PPI determines how many dots the laser beam will place per inch. I'm pretty excited to see the results and if there's any surprises.
![material scale file](../images/week03/illustrator-material-scale.png)  
  

![Laser scale test](../images/week03/laser-scale-hdf.jpg)  
***The result of the laser settings test***  
*Cutting*: You want as few burn marks on your material (both sides) as possible but the material should still have a nice clean cut all the way through. With that in mind, and looking at my test, it seemd that 100% power and 14% speed was the best result.  

*Engraving*: HDF is a bit "difficult" to engrave as the material doesn't vary that much in color when burned. But my scale gives a nice overview of dept and color. I had a minor mistake when (forgetting) to ajuust the origin on the lasercutter for the 60% power engraving, but I was able to pause and fix it pretty fast.  

*Kerf*: I made two tests. I drew a 20x20 mm square for testing the lasercutters kerf. The kerf is important, as you need to know how much material the laserbeam burns away when cutting a line. You will end up with parts that doesn't fit together if you don't take kerf into account in your design. The kerf will vary according to which materiale and machine you use.  

My test showed that my inner square (A on picture) was 19.9 mm, and therby lost 0.1 mm to the laserbeam. The hole (B on picture) was 20.1 mm and where thereby 0.1 mm bigger (0.05 mm  on each side of the square) than the drawing I made. I concluded that the slot I have to keep in mind, while designing my press fit construction, is 0.1 mm.  
![Laser kerf square test](../images/week03/kerf-square-test.jpg)  

For my second press fit test I made five slots in different sizes and cut two of them. I then tried to fit the slots together in the same size. I appears that 2.9 and 2.8 mm fits the best.  
![Laser kerf test](../images/week03/laser-kerf.jpg) 


I also tested the lasers focus by drawing three squares with an A inside and some text above. The first square are focused too close to the material, no. 2 are in focus and no. 3 are focused too far away from the material. It's easy to see the text gets blurry when the laser are unfocused and the engraving doesn't get as deep. Another detail is the square have nice sharp edges on the focused square, but on the others it gets more rounded on the edges. I used the focus tool and the arrow-buttons in front of the laser to adjust the Z-axis up and down to get the different heights.  
![Laser focus test](../images/week03/laser-focus-test.jpg) 



### Drawing the contruction kit
![OnShape part overview](../images/week03/onshape-part-overview.png)  
***My final overview of the jewelry tree parts***  
In OnShape I started my drawing by defining a varible for the slot length and width. This way I can change the variable and OnShape will update all the messurements where I put it ```#slot``` or ```#slotlength```. I also made a varible for the material thickness, so if I later on choose to change the material to a thicker material, I can just edit the messurement in one place.
![Making variables in OnShape](../images/week03/variables-onshape.png) 

I drew the tree spine (part A+B) to get the base structure done, later I added the bottom plate (part C). For all my objects I applied constraints where possible - e.g Parallel, Use, Symmetric, Perpendicular, Vertical and Horizontal. In this way I can secure that my drawing are fully constrained.  
I drew a rectangle with a 2.9 mm wide slot as I experienced in the test will give the right tight fit. I added chamfer to the corner of the slots so that the parts will slide better into each other. Just to be sure that it fit nicely, I cut the spine and rectangle to test it. And actually it was a bit too tight, as it could not be dismantled without putting a lot of force in to it and then it broke.  
![Broken parts](../images/week03/broken.jpg)
![cutting another test](../images/week03/lasercut1.jpg)

So I adjusted the slot by +0.1 mm and cut a new test. I was then able to assemble it and disassemble it without breaking the parts, and it still fitted tight. It seems that the material varies a tiny bit, as some of the parts sometimes fitted more tight eventhough I tried to place it in different possitions, but it is not a problem.  

With the slot messurements in place I drew three parts (E+F+G) to hold earrings, necklaces, rings and so on.  
![Long earring part](../images/week03/earring-long.png) ![insert slot variable](../images/week03/slot-variable.png)  

When assembled the parts, I found that I also needed a long part (part D) that could place the jewelry further away from the short parts and also allow the option for building the tree upwards. I made it a Y-shape so the tree can spread out. To make the arms in the Y-shape stronger I added ```fillet``` to the inner angles.  
I also made a short extention part (part H) as I experienced that the tree needed to be much more versatile and make more space between the jewelry-holder parts for more easy access.  
![OnShape 3D model](../images/week03/onshape-tree1.jpg) ![OnShape 3D model](../images/week03/onshape-tree2.jpg) ![OnShape 3D model](../images/week03/onshape-tree3.jpg)  

###Learning outcome
I´m pretty happy with the result, and it can be assembled as needed. If I had more time I would had like to make it more pretty and organic shaped.  
I'm a bit surprised that eventhough I made the kerf and press fit tests, I still needed to adjust the messurements as they where too tight for this task. The lasers in the lab are used by many different people, and that's why you always have to set the focus on the laser before doing a job. The problem with that is that to make an accurate press fit I have to have the exact same distance between the material and lens each time. And with the manual setting of the focus, it vary a lot eventhough I'm trying to get the same setting. That affects my result if I cut my kit in diffent days, as the parts sometimes don´t fit while being to loose or to tight. It also seems that even small variences in material thickness can have an impact on your project if you make it very accurate.  
I got confirmed that it's always a good idea to test small parts (as e.g connections, slots) before making the whole project, as you can more easily fix any problems. In my final project I need to keep that in mind while working with all my different parts and materials.  

This is the result og my Jewelry tree press fit kit! I tried to place as many different types of jewelry that I had. I think it's great that its so versatile and that you can add or remove parts so the the fits your needs.  

![heroshot of my jewelry tree](../images/week03/heroshot_1.jpg) ![heroshot 2 of the jewlery tree](../images/week03/heroshot_2.jpg) ![package of my press fit kit](../images/week03/pressfitkit-sheet.jpg)  

#Image resize problem
I had a hard time scaling some of the images after I uploaded them in the wrong size. It took some time, but I found a solution and descibed it here: [Project management - website buildup](https://hellehauskov.gitlab.io/students_template_site/assignments/week01b/#website-buildup)

# Files
*Vinylcutting*  
[Cut file (EPs-file zipped) for Vinylcutting](../files/vinyl_doors.zip)  
[Cut file (SVG) for Vinylcutting](../files/vinyl_doors.svg)  

*Laser test files*  
[Illustrator scale file](../files/laserscale-hdf.ai)  
[Scale file pdf for the lasercutter](../files/laserscale.pdf)  
[Link to kerf test Onshape-file](https://cad.onshape.com/documents/cb9b7bf4c4586b61a7e9c140/w/dc6c284c8a229b2df0e686a5/e/a6fdd1427ae6085aae94ddb9)  
[Kerf test finger pdf](../files/kerf-test-fingers.pdf)  
[Square kerf test file 20x20mm AI](../files/laser-square-20x20mm.ai)  
[Focus test file AI](../files/focus-test.ai)  

*Jewelry files*  
[Link to onshape file](https://cad.onshape.com/documents/e3f63e021c121d4d3d370e19/w/5cd43e02d83d2b2d1dc0060c/e/dece78ca047eb9015475c990)  
[STL file jewelry tree](../files/jewelry_tree.stl)
[Cut file for Jewelry tree (PDF)](../files/jewelrytree-cutfile.pdf)  
[Design file for Jewelry tree](../files/jewelrytree-cutfile.ai)  



