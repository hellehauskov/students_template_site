# 3. Computer Aided design

Week 2 assignment: Model (raster, vector, 2D, 3D, render, animate, simulate) a possible final project in as many ways as possible. Compress your images and videos, and post it on your class page. Try different programs and find the ones that suits you.

I felt confident that my workload this week wouldnt be as heavy as last week, as I have some experience in 2D/3D. But I was wrong.

I started by choosing the programs that I would like to try out this week:
2D: Gimp for the raster experience and InkScape + Adobe Illustrator for the vector part. 
3D: Onshape, as I have some experience with this great program and would like people to know it excists. I also will also try out FreeCad and Fusion 360.
Rendering: I will try out Onshapes rendering feauture if I have the time. I have never tried rendering before.
Video: OBS Studio for recording videos and Handbrake to help with the compression.
You will find links to the programs at the buttom of the page.

----
# Gimp
I have just installed this free open source program. It's seems like paint in advanced mode and they call it an image editing program. It have many of the icons known from Photoshop. I can paint with a brush, write text and of course edit a picture. But it is all raster (pixels), so I will never be able to scale my object without the image will start to pixelate. So if you paint a picture or make something in a program like this, you need to check from the start if the canvas is the final size you need (in all the sizes you may need this file in the future.)

If you need an object (e.g. a logo) to be able to scale limitedless, you need to draw your graphics in InkScape, Illustrator or another vector based program. Then the lines are mathematically calculated all the time and will thereby also be sharp no matter what size.

## Image in text image (raster)
I would like to try make some text filled with an image. I found a [Youtube tutorial by Chris "Put Image Inside af Text Shape"](https://youtu.be/sH-sBi6QcaA), and watched it several times and then tried it for my self.
At first I had a bit of trouble as my GIMP was installed in Danish, so the toolbars took a bit of time to translate.  
  
Anyway, the process is pretty simple.  
First you go to `files` - `new` - and type in the desired size for the document.  
![Create new document](../images/week02/gimp-1.png)  

Then you just pull the image file from pathfinder in to the GIMP document you just made.  

On your right you have the Layer Panel - it's like having multiple pieces of paper on top of each other. You can then work on each of them, change the order of the layers, hide and show layers and so on.  
Right now there is a background layer and an image layer.  
![Layers panel](../images/week02/gimp-2.jpg)  

In the Layer Panel, click on the eye next to the image layer. Now the layer are hidden.  
Type in your text by using the text tool 'A' on the left upper corner. You can also ajust font and size on the left.  
![insert text](../images/week02/gimp-3.jpg)  
Right clik on the text layer in the Layer Panel on your right. Select `Alfa to selection`.  

Then right clik on the image layer and select `add layer mask`.  
**Important**: Choose `Black full transparancy` and clik `ok`.  
![Layer mask](../images/week02/gimp-6.png)  

Click on the image mask in the Layer Panel to activate it. It is a black square next to the image.

Use the Bucket tool (upper left corner) and paint white by clicking on the letters.  

Clik on the eye next to the text layer to hide it. The text will show as a selection with dotted lines.
![Hide layer](../images/week02/gimp-7.jpg)  

Then click on the eye next to the image layer to make it visible.  
Now you should see the nice result of text with the image inside. You can use the move tool to move the text above another area of the image.  
![Hide layer](../images/week02/gimp-8.png)  

And here is the final image:
![Hero shot](../images/week02/gimp-image-in-text.jpg)  

## Painting a raster image  
I selected the brush tool and ajusted the size to 64 px, chose a purple color and a Hardness brush.  
![Brush tool](../images/week02/gimp-9.png)  
Then I started drawing my coffee cup. Not that easy with a mouse, so honestly not the most beautiful drawing I have ever made.  
I ajusted the brush a bit in size for some of the "steamy" strokes. My aim was to make kind of an icon, and that's why I didn't work with the more artsy brushes. 
To save the file in GIMP format, choose `File` - `save as`  
If you want to save the image in e.g. bmp, png, jpg, choose `File` - `Export` - select fileformat.  

To save my file I had to export it to get some known fileformats. I exported it as an .bmp file. 

![Gimp coffee cup](../images/week02/coffeecup-gimp.png)  

If you want to know more about raster and vector - and all the fileformats, I think that [Shutterstock](https://www.shutterstock.com/blog/raster-vs-vector-file-formats) has a good explanation.  

// Later I found a way of changing the language in GIMP. Go to `edit` - `preferences` - `Interface` and then change the language.

----
# InkScape
This is a free program that makes vector graphics. It is a worthy competitor to Adobe Illustrator. The program gets more and more functions on each update that you normally only get if you have the professional programs. It is a great program and you can find a ton of tutorials to get you started.

I want to change the bmp-file I made in Gimp from raster to a vector file. In Inkscape that's quite easy and with a fine result. 
I opened the file and then press `Shift+Alt+B`. Remember to have your object selected. 
This is the Trace bitmap dialog box.  
![Inkscape trace bitmap](../images/week02/trace-bitmap.png)

I ajusted the brightness treshold to 0,980. If it's too close to 1 the preview get's black. Then I pressed `update`. Now InkScape has redrawn my coffe cup by tracing the picture. 
Now I have two pictures that looks the same.  
  
![Inkscape compare pics](../images/week02/inks-compare.png)

But if I zoom in, it reveals that the purple cup are raster (pixelated) and the black cup is vector (sharp).  
![Compare raster vs. vector image](../images/week02/rastervector.png)

I then deleted the unsharp image.

Because the file now are vectorized, I'm able to ajust the lines with the Node tool `N` so my shakey lines can be corrected or e.g. move the handle lower.
![Inkscape edit nodes](../images/week02/ink-edit-vector.png)

----
# Adobe Illustrator
I use Illustrator for making vector graphics (AI and EPS files). It is a professional program usually used by graphic designers and are a bit expensive.  
![Vector drawing made in Illustrator](../images/week02/lamp_vector.png)
This picture show a vector drawing I did in Illustrator with the Pen tool. Drawing programs (raster/vector) are hard to get precise e.g. CAD programs. But still you can make files for lasercutting if its not too complex e.g. cutting objects that are 2D. If you want to do some 3D objects you should use CAD programs instead as they are made for it and will help you do the math. And simply - there are some shapes that you can't reproduce in a 2D program.


![Illustrator vectorhandles](../images/week02/ill-vectorhandles.png) ![Illustrator](../images/week02/ill-pen.png)  
The red line are the vector handles. It determines how the line bend towards the next point and towards the point before. You make the lines by selecting the Pen tool in the menu. It takes a little time to master, but when you do it's the most valuable tool in the program in my opinion. The tool are also used when making clipping paths in Adobe Photoshop. 


![Illustrator fill stroke menu](../images/week02/ill-fill-stroke.png) If you click on these icons you choose 'fill' or 'stroke'. If the square are in front you can double clik and choose a color for the fill, and vice versa if you press the square line. The red line across indicates that transparent are choosen. 


----
# FreeCAD
After having a hard time trying to understand FreeCad (and spent way to much time on it), eventhough I watched this tutorial [FreeCAD for beginners p.1](https://www.youtube.com/watch?v=uh5aN_Di8J0&feature=youtu.be) many times. I got stuck several times. For instance I could not see all the constrains that my square had got. The line got attached to the x-axis by mistake, but i wasnt able to select the constraint as remove it. I deletede the drawing and tried again a couple of times but some other simple actions stopped me and my workflow was really slow.  
I surrendered and turned to OnShape that I'm familiar with, so that I had something to show for this week. FreeCad works way diffent than I'm used to and feels a bit complicated and diffcult to figure out. I'm sure I can learn to use the program but I have the feeling that it will take me a lot of time.

![My test in FreeCAD](../images/week02/FreeCAD-test.png)

----
# Onshape
OnShape is a browser based CAD program. I think it is a great program, very intuitive and a lot of functions. I haven't practiced very often, but I'm able to draw some reasonable 3D drawings. It was nice to feel that there was finally something I had some experience with. In our Fab Lab, we teach students, makers and guests to use OnShape as it have the simple user interface and works perfect eventhough it's a browser based program.  


I started sketching my final project - the lamp.  



*Constraints* and *dimensions* added to my skecth. The lines are shifted from blue to black as the skecth is now fully constrained. This is important as it makes my sketch parametric.  

![onshape constraints](../images/week02/onshape-constraints.png)  

  
I used *fillet* to make a more elegant curve where the lamp had a sharp curve.
![onshape fillet](../images/week02/onshape-fillet.png)  

  
At the top of the lamp there was sharp curves too and I used *fillet* to round those. I think that if I had kept the shap edges, they would be more exposed and likely brake when the lamp is made. And I think it gives the lamp a more finished look.
![onshape fillet on top edges](../images/week02/onshape-fillet2.png)  


I had to *split* up the lamp base as I don't want the LEDs to go all the way down to the floor. And I would like to make the bottom part in another material (maybe concrete to make it heavy). 
![onshape split object](../images/week02/onshape-split.png)  


I hid Part 1 (the bottom) so that I could make a drawing on the new bottom face. This drawing wil later on be used to remove material from the main object (part 2). 
![onshape split new sketch on face](../images/week02/onshape-sweep1.png)  


The LEEDs are inteded to be countersunk in to the lamp, so I used the drawing to make a *sweep* along the edge of the lamp.  
![onshape split sweep by edge](../images/week02/onshape-sweep2.png)  


Now there's hopefully room for the LEDs in the trench.  
![onshape split sweep done](../images/week02/onshape-sweepdone.png)  

I edited the appearance of the parts so that they got diffent colors.
![onshape-appearance](../images/week02/onshape-appearance.png)  

The finished look. 
![onshape final object](../images/week02/onshape-final.png)


I made a short video of the workflow:  
<video width="640" height="360" controls>
  <source src="../../video/onshape-flow.mp4" type="video/mp4">
Your browser does not support the video tag.
</video>  


Finally I created a *drawing* of my lamp parts, to show the diffent angles. 
![Onshape drawing sheet](../images/week02/onshape-sheet.png)



----
# Fusion 360
Fusion 360 is a CAD-CAM program. You install it on your computer but saves the files in the cloud. 
My Fusion 360 license was expired, as I installed it over a year ago but never used it. I applied for a new license, but it takes time to get approved with the documentation. Meanwhile I made a new account with a different e-mail address (a 30-day trial version) so that I can get this weeks assignment solved.  

To save time I started out by watching Lars Christensens [Fusion 360 Tutorial for Absolute Beginners— Part 1](https://youtu.be/A5bc9c3S12g) on Youtube, as he get's highly recommeded by the makers I know. It's a great video and he is very good at explaining what to do.  

I tried to draw the same object as he does in the video. It went okay and I learned som functions that I didn't know in advance. I will describe some of them below:

Fusion can extrude in both directions but in diffent lengths in the same process - and have a smart function that closes gaps bewteen two figures (as you can see on the picture below in the blue fields on the right). That's pretty smart, unfortunately I don't think its a function that you would be able find on your own. 
![Fusion 360 extrude directions](../images/week02/fusion1.jpg)  


Another cool feature I learned was that if you move the mouse over a 'hidden' line, it tries to guess what you want to select and highlight the hidden line. Difficult to describe, but if you look at the picture below you can see that the line in the corner are hidden in my viewangle but are highlighted as Fusion tries to help.  
![Fusion 360 extrude](../images/week02/fusion2.jpg)  


The third thing I think is quite helpful when you are new at 3D drawing, is the 'Model toolbar' (hit **S** on the keyboard). Type in the name of the tool, search and the select the tool from the list. Easy! You can also add the tools you use the most, next to the predifined ones in the Model toolbar.  
![Fusion 360 extrude](../images/week02/fusion4a.png)

The last thing I would like to mention are the Section Analysis in the 'Inspect' toolbar. It makes a cross section of the part you select and you can even adjust how deep. I had a some difficulties to turn the view off afterwards, but I have only used the program for a couple of hours so I'll get the hang of it later on.

I think that Fusion are easy to learn - especially if you have tried Onshape or similar CAD systems before, as you have an idea of which function you are searching for. The user experience are better than FreeCAD but not as simple as OnShape in my opinion. 
I know that Fusion is the program I'll try to use when we get the CAM assignments, and therefore it makes sense to try it out already.  


----
# Fusion 360 vs. OnShape
I thought Fusion 360 was easy to use and that I didn't need any tutorials at first. But I did. Eg. I wasn't able to figure out how to orbit as my OnShape-habits sabotaged that. In Fusion you press the scroolwheel and shift at the same time. In OnShape you just press your right mousebutton. Both programs goes from blue lines to black when the drawing are fully constrained.  
So I had a bit to learn as Fusion seems a bit more engineer-level and complex in the user interface whereas OnShape has a more simple structure. OnShape also have help-messages popping up and literally tells you what to do. Fusion do too but I didnt always find the helpful enough to guide me. I know that Fusion have a great CAM part and OnShape don't provide the CAM.


----
# OBS Studio and Handbrake
OBS Studio is a faily advanced video recording program. It has a lot of cool features. I installed this as I would like to make video recordings to show some of the workflows I'm doing in this assignment.
![OBS black screen](../images/week02/obs-blackscreen.png)

I installed OBS Studio without any problems, but when I open the program my screen was black. I tried a lot of different things, but I quickly got the solution on the [OBS Wiki page](https://obsproject.com/wiki/Laptop-Troubleshooting). It was my laptop graphics settings that needed ajustment.

Finally working, I tried to find a good tutorial - I can really recommend [Complete OBS Tutorial for Teachers, Trainers, and Video-makers](https://www.youtube.com/watch?v=wO2gswOEVAQ&feature=emb_logo) by Joe Milne!! I was surprised to learn how professional the program is, despite it is free. You can make a lot of effects, transitions, green screens and have multiple pictures/videos showing at the same time. I hope that I will have time and a project that will acquire I learn more of these skills. I would also like to know how to add music but I guess that it will have to wait until I have more room for that.
Anyway, I have learned how to stop and start a video recording of my browser, and that was what I needed as a minimum.

I tried to use OBS to record my workflow in OnShape, but I could not record the whole frame from the broser (see picture below). I still don't know what happend, but I got success later. 
![OBS broser fail](../images/week02/obs-browsererror.png)


Tried to use Camtasia, as I ususally use for my instruction videos. That worked well. But the file size was too much - 14MB and this was only half the video! So that wasn't an alternative.
I downloaded HandBrake and watched a tutorial [How to Compress a Video File without Losing Quality](https://www.youtube.com/watch?v=sgmE1T8A4UY&feature=youtu.be) which tells you how to compress a video with HandBrake. It was a great tutorial, but my file only lost 2 MB! What to do?! The goal is 1MB as Neil recommended..


I then tried OBS again later and made just a very short video of my work in OnShape. And fortunately the file was just about 2MB - I tried to keep the video short and had muted the sound. I gave HandBrake another try and as a result the video reached 294KB!! Success! I needed that on a late Friday evening! Now I'm concerned about how to get a preview on my website as I don't know to do that!  

I got help from <https://www.w3schools.com/html/html5_video.asp> where I could copy the HTML code for video, and after struggling with the rigth path, it worked! Then I realised that the resolution of my compressed video was too low.

![HandBrake settings](../images/week02/handbrake-settings.png)

 I made a new compression in a higher quality, and it ended just below 1MB. I also increased the size of the video width and height in the code.

```
<video width="640" height="360" controls>
  <source src="../../video/onshape-flow.mp4" type="video/mp4">
Your browser does not support the video tag.
</video>
```



**NOTE** to my final project: I realised during this weeks process that maybe the foot stand shouldn't be a hemisphere but instead integrated in the post - maybe as casted concrete? Or epoxy? I realised that my project aren't that hard to draw in 3D yet. But I still have to figure out about the electronics placement and how to mount the footstand, these will definitely change the complexity of my 3D drawings.

----
# Files
[GIMP file](../files/coffeecupgimp.xcf)  
[InkScape file EPS](../files/coffeecup.eps)  
[InkScape file SVG](../files/coffeecup.svg)  
[Illustrator AI file](../files/lamp.ai)  
[Onshape linked file](https://cad.onshape.com/documents/2bdf1d484230211b296eb285/w/d1caa20bcd98a7630dfecdc5/e/80d4e12e1f60c978a8d2b546)  
[OnShape STL](../files/Lamp.stl)  

----
# Links

[Gimp](https://www.gimp.org/)  
[Inkscape](https://inkscape.org/)  
[Adobe Illustrator](https://www.adobe.com/products/illustrator.html?promoid=PGRQQLFS&mv=other)  
[OnShape](https://onshape.com/)  
[Fusion360](https://www.autodesk.com/products/fusion-360/personal)  
[FeeCAD](https://www.freecadweb.org/)  
[OBS studio](https://obsproject.com/)  
[HandBrake](https://handbrake.fr/)