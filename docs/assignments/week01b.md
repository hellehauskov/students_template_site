# 2. Project management

I have zero knowledge on this weeks assignment on building GIT and a website and I therefore kept to the basics.

I have studied a lot of former FabAcademy students websites to get inspiration for my own website and also to get an idea of how this weeks task can be done. I will stick to the basics and hope that I in time will be able to 'upgrade' my site and make it more interesting and reflect who I am.

With great guidance in the local class from my local instructor Iván Milara, Fab Lab Oulu, and from my colleague, Hans Peter Haastrup Nielsen, I had an idea where to start. I installed GIT, Python, Notepad++ and MKdocs.  

As this is my first time creating a webpage from scratch and have no experience in HTML, I decided to use MKDoks as my web static generator application. It's advised from the FabAcademy as HTML takes quite some time to learn and master. MKDocs generates the site and provides some templates. The documentation source files are written in Markdown. The MKdocs and Markdown converts my site written in Markdown Syntax to HTML. Markdown is a simplification of HTML and are thereby easier to learn as the Markdown codes are more simple (but also limited). By using Markdown I can build content without knowing much about HTML coding and still build up a structure and layout for my webpage.
The site configuration are saved in a YML file. I use Notepad++ to edit the markdown files.  

## Learning GIT by tutorials
I'm quite a visual person, and learn best by watching. So I watched different GIT tutorials on Youtube.
The youtube-tutorial [GIT tutorial for beginners: Command-line fundamentals](https://www.youtube.com/watch?v=HVsySz-h9r4) by Corey Schafer, took me through the difference between Central VCS and Distributed VCS (GIT). I also learned about the `git help 'word'` command. It will come in handy!

Learning by doing and failing. I was scared to change anything and write commands - what if I broke it all?? After staring a bit I went ahead and started trying. Most times I acutally did well. The most errors was the misspellings or being in the wrong directory. 

### Git ignore tutorial
I followed the [Git Tutorial 8 - .gitignore files](https://www.youtube.com/watch?v=ErJyWO8TGoM&feature=youtu.be) on youtube, made by Codebasics. It was a great basic video and I learned what the gitignore files is and what it does. I tried it out and it worked right away. If you have a file that you don't want GIT to track, you open the gitignore file, and add the filename to the ignore list. Of course you have to add, commit and push your ignore gitignore to keep your remote repo up to date.

## Website buildup
A special thanks to the local class with Iván! It gave me a good idea on how to build up the site. Oulu University had also great guides on how to get started and whats needed on the site.
My first problem was that I didn't have a student site on Fabcloud as I got it to FabAcademy just two weeks ago.
After instructions from my local instructor I created an account in GitLab.com and will use that untill I have FabCloud access.
Then I logged in and forked the Oulu template site, so I had a structure to start my repo with.

### How to create a SSH-Key and upload it
To communicate safely with my remote GIT repository I had to create a SSH Key. It's a generated private key that identifies me, and thereby gives me access to my files. You may never share your private key with anyone, but you can share the id_rsa.pub file. Be sure it's the .pub file!
I downloaded GitBash as recommended from my local intructor Iván Milara.

1. Type in `ssh-key -t rsa -C "your.email@example.com" -b 4096`. (remember to change the email adress to your own)
2. Save the file as GitBash suggests: `users/name/.ssh/id_rsa`
![generating ssh-key](../images/week01/gitbash-ssh-key-generation.png)
3. Make a password - if you don't want one, just hit 'Enter'.
4. Type the same again or 'Enter'
5. Open Pathfinder and find the path with your SSH-key in. The files is called id_rsa.pub. It should look like this:
![SSH-Key in pathfinder](../images/week01/ssh-key-path.png)
**Be sure that you open the .Pub file!** It's the public version of you SSH-Key. Again - never share the other id_rsa file - thats you private file.
6. Open the SSH-key in Notepad++ and copy all the text.
7. Go to GitLab and login
8. Click on the icon placed in the upper right corner (your photo) and go to settings
9. Find SSH in the sidebar and insert the copied text and press Add.
![SSH-Key in GitLab](../images/week01/ssh-key-Git3.png)
You will recieve an email confirming that you added the SSH-key.

*YAY! You are done.*
 
### How to clone
I made a folder in pathfinder for the local repository.  
Go to gitlab, press 'clone' in your repossitory. `"Clone with SSH"` copy the text. 
![GIT clone URL](../images/week01/GIT-clone-URL.png)

In Command Prompt: go to the path of the new folder of your local repository.
![GIT clone](../images/week01/GIT-clone.png)  

I went back to the Command Prompt and started the GIT configuration:
`git config --global user.name "my name"`  
`git config --global user.email "my email"`
`git config --list` Here I can check my name and email appears correct and it looks like success.
![GIT configuration](../images/week01/GIT-config-email.png)


### How to edit the text
Then I started to edit the md-files with Notepad++. I have the Command Prompt open all the time and starts by writing `mkdocs serve`.  ![MKdocs serve](../images/week01/MKdocs-serve.png)  
Then I get a preview of the site by opening my browser and type in `http://127.0.0.1:8000`.  
As long as the CMD are running, my changes are updated as soon as I save the file...

**TIP** I learned that your site fails if you close the cmd. That took some time to figure out. ![error message loading local page](../images/week01/error-webpage.png )

I have seen that others use `emphasised words` and I thought that it might help the readability on my site. So I looked at the [Markdown cheat sheet](https://www.markdownguide.org/cheat-sheet/) and found the Markdown syntax.
I had problems making forced lineshifts, but I found [Shaunlebrons](https://gist.github.com/shaunlebron/746476e6e7a4d698b373) example, and learned that I just need to press Space twice. Very easy!
I tried change the photos in the template and had great succes by copying the code `![text that shows if picture fails](../images/week01/filename)`. Remember to spell corectly and accurate with uppercase/lowercase letters. 

### GIT commands
When I'm done editing the file, I have to get my changes to the repository.
**TIP: If you write cmd in your pathfinder path it opens Command Prompt in the folder you where in.**

**CLONE** The command `git clone` creates the local repository on your computer by cloning the original repository remote.  
**STATUS** The command `git status` gives you a current status of my repo. The list shows in which state my files are and in which branch Im located in. It also tells me if the local repo is in sync with my remote repo. Then its easy to see if need to take action on some of my files.  
**ADD** The command `git add filename.md` adds you local working file to the Stage and tracks the file.  
**COMMIT** The command `git commit -m "example text"` saves a snapshot of the files from stage with name, date, time and comment and stores it in my local repo.  
**PUSH** The command `git push origin master` pushes committed files from local repository to remote repository. The files are now updated in the remote repository.  
**PULL** The command `git pull origin master` updates your local repository with the newest version from the remote repository.  
 
This is the workflow in the Command Prompt: 
1. `Git pull origin master` start your work by updating your local repo with the newest version from the remote repo.
2. `git status` This way you can check the status on your files.
3. `git add filename.md` When you are done editing your files you need to add the file to the stage.
4. `git status` check the status on your files. Is everything as expected?
5. `git commit -m "changed pictures in filename.md"` Now you need to write a comment that explains what happend to the file. When you commit the file it saves a snapshot of the changes to you local repo. 
6. `git status` check the status on your files. Is everything as expected? 
7. Now you need to push the commit (usually you have multiple commits)`git push origin master` so it integrates and updates the remote repo. 
8. `git status` check the status on your files. You should get a message that you are up to date with the master.

Here you see my first `push`to the remote repository  
![My first push to remote repo](../images/week01/GIT-push-first.png)


###What did I change in the YML-file

I chose one of the preinstalled themes that MKdocs have, so it was easy to change the theme just by changing the theme-name in the YML-file.  
I changed the theme from Material to Readthedocs, as I like the navigation and layout much better.
I have changed the site_name, site_author, copyright, theme and color.
I would really like to change the blue color i top left, but I have still not found a tutorial that help me do that. Or to change any color at all. But I just found some knowledge about custom styling on the [mkdocs website](https://mkdocs.readthedocs.io/en/0.15.1/user-guide/styling-your-docs/#custom-themes) that I will try out as soon as possible.

![Changes in the YML-file](../images/week01/yml-changes.png)

###Requirements.txt and the gitlab-cy.yml
The gitlab-cy.yml file contains configurations. Here is defined the scripts you want to run and eg.other configuration files and templates you want to include.
It also contain information about the location to deploy your application.

My gitlab-cy.yml file looks at requirements.txt before running the script. So the txt-file have information on what needs to be installed before building the mkdocs.  
From the gitlab-cy.yml file:  
```
image: python:3.8-slim

before_script:
  - pip install -r requirements.txt

pages:
  script:
  - mkdocs build
  - mv _site public
  artifacts:
    paths:
    - public
  only:
  - master
```

###Resize image problem
I had a problem with image size (in the [Computer controlled cutting week](https://hellehauskov.gitlab.io/students_template_site/assignments/week03/)) as I uploaded an image in the wrong size. It was displayed too big. To avoid pushing new pictures in the right size to my repo, I wanted to figure out how to resize the image by HTML code. I've searched the internet for some solutions. No matter what HTML codes suggested, it failed. But I knew that there must be a solution out there!  
My colleague, Hans Peter Haastrup-Nielsen, suggested to insert: `{ width=50% }` at the end of the image code.  
The result:  `![material scale](../images/week03/illustrator-material-scale.png){ width=50% }`  And it finally worked! My image are now displayed in 50% size, and I don´t need to push resized images to GIT and taking op more space than they already do. What a relief!
