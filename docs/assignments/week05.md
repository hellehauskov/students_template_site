# 6. 3D Scanning and printing  

*Group assignment*:  
Test the design rules for your printer(s).
Document your work and explain what are the limits of your printer(s) (in a group or individually)

*Individual assignment:*
- Design and 3D print an object (small, few cm3, limited by printer time) that could not be easily made subtractively.
- 3D scan an object, try to prepare it for printing (and optionally print it).

#Testing the designrules of Prusa MK3S
For this task I used the Prusa MK3S printers that are available in the Fab Lab. The printarea on a Prusa MK3S are 21 x 25 x 25 cm, and the extruder are 0.4 mm.  
I used ["Test your 3D printer" file](https://www.thingiverse.com/thing:1363023) and some of the files from [FabAcademy class notes](http://academy.cba.mit.edu/classes/scanning_printing/index.html) to test the limits of the 3D printer without using suport structure (minus the clearance-test).  


##Dimensions  
The printer printed perfect according to the meassurements. There is no difference in inner or outer dimensions on the print compared to the digital file.  
![inner dimension](../images/week05/dimensiontest_inner.jpg) ![Outer dimension](../images/week05/dimensiontest_out.jpg)  

##Bridging  
This object tests how long the printer can print between to colums - a bridge. The file contains 10 bridges from 2 mm  to 20 mm in length. The Prusa printer manage the job just fine and there is very little matrial that hangs beneath the largest bridge.  
![bridging](../images/week05/bridging.jpg) ![bridging2](../images/week05/bridging2.jpg)  

##Clearance  
The clearance test explores how much clearence you have to keep, in order to keep the objects from melting together when printing. By mistake I printed the test without support, and the result speaks for it self.  
![clearancefail](../images/week05/clearance_fail.jpg)  
I then added support in the Prusa Slic3r by painting on the support on the desired areas. I didn't want support inside the hole in the squares.  
![Slic3r](../images/week05/slic3r1.png) ![Slic3r](../images/week05/slic3r2.png)  
My test shows that 0.4 mm actually is enough cleanrance to keep the objects from becoming one part and are able to move independent from each other.  
![clearance](../images/week05/clearance.jpg)  ![clearance2](../images/week05/clearance2.jpg)  

##Overhang  
*Angle:*
Here I'm testing how sharp an angle on an object, that the printer can print without the filament starts to hang or get an ugly finish. At an angle of 60 degrees you start to see the finish of surface isn't that pretty, but at an angle of 40 degrees the filament starts to hang and lumps together. So the conclusion is that if you want a perfect object you will have to stick to an angle of 70 degrees and above.  
![overhang test angle](../images/week05/overhang1.jpg)  

*Length:*
I also testet the lenght of an object - that is a beam that goes straight out in the air in an 90 degrees vertical. On the picture below you can clearly see that the printer allready gets in trouble when the beam is 2 mm long and that it fails completely by 5mm and above. This is because the printer, of course, are not able to print straight in to the air, but needs support so there's something to build on.  
![overhang test length](../images/week05/overhang2.jpg)  

#Wallthickness og slot  
This little test shows that when printing thin objects you have a limit of 0.4 mm in wall thickness. Simply because the extruder is 0.4 mm and naturally can make smaller things than that. The slot test shows that 0.2 mm slots 
are the the smallest slots the machine are able to make without the walls melts together.  
![wallthickness and slot](../images/week05/wallthickness-slot.jpg)  

#Surface finish  
The test shows that when printing a curved object, the layers are more visible and on the top or bottom of the sphere the printer are not able to replicate the curve exactly because of the layer height.  
![Surface finish](../images/week05/surface.jpg)  

The "Test your 3D printer" file tests the printer in all the parameters that are relevant for 3D printing objects. The result of my print shows that the Prusa MK3S does quite a good job and didn't fail any of the objects.  
![Thingiverse test file](../images/week05/thingiversetest.jpg)  


#3D printing my own designed object
I designed my object in OnShape. The first thing that came in mind where a cube with an sphere inside. But from the lectures I heard that the task needed more than just that. So then i decided to make a cylinder that has an object around it, but are able to turn all the way around. This is acually a task that 3D printing can solve without assembling the parts afterwards.  

First I started by drawing the cylinder with a center tab and then used revolve to make the drawing symmetric all the way around.  
![Cylinder drawing in OnShape](../images/week05/onshape1.png)  

Then I drew the object around the cylinder with a clearance of 1.5mm so the parts can move a lot. 
![Cylinder drawing in OnShape](../images/week05/onshape2.png) ![Cylinder drawing in OnShape](../images/week05/onshape3.png)  
I decided to make a flange in 30 degrees that revolves around the cylinder and using a circular pattern I made three flanges that connects in a circle around the center tab.  

![Cylinder drawing in OnShape](../images/week05/onshape4.png)![Cylinder drawing in OnShape](../images/week05/onshape5.png)![Cylinder drawing in OnShape](../images/week05/onshape6.png)![Cylinder drawing in OnShape](../images/week05/onshape7.png)  

To ease the printing I extruded the center tab end plate up to face of the outer construction. This way all the parts touches the headbed on the printer and makes the printing hopefully more successfull (and then it doesnt need support underneath). This way you can make the cylinder turn inside the construction, and I am still able to get access to remove the support material after printing. (I still had some trouble removing all support from the center tab and the small areas as my tools where bigger.)  
![Cylinder drawing in OnShape](../images/week05/onshape8.png)  

 I added a Fillet to the top of the flanges to get a nice finish.  
 ![Cylinder drawing in OnShape](../images/week05/onshape10.png)  

This object can not be easily made subtractively, as there's areas that the milling machine would not be able to reach. 
To have a better result I should have added support all over the object. Then the top part of the cylinder would have had a more smooth finish at the underside. I could also have closed the outer construction even more to make it even more difficult to mill.
![Cylinder drawing in OnShape](../images/week05/myobjectarrows.png)![Cylinder drawing in OnShape](../images/week05/onshape11.png)  








#3D scanning an object
I have never tried 3D scanning an object, so this task is a brand new area for me. 


