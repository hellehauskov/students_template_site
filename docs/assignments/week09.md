# 9. Embedded programming
Group assignment:

Compare the performance and development workflows for different microcontroller families
Document your work (in a group or individually)





In the group work we compared the performance and the development workflows for the ESP32 (Kenichi), STM32 (Mona), xxxx (Antti) and Arduino Uno (Helle).

#Arduino Uno  
I inserted the code (below) from our instructor Gleb Bulygin, in Arduino IDE.
```
void setup() {
  pinMode(2, OUTPUT);
  pinMode(3, INPUT);
  digitalWrite(2, LOW);
  // put your setup code here, to run once:

}

void loop() {
  digitalWrite(2,!digitalRead(3));
  // put your main code here, to run repeatedly:

}
```

Then I  selected in the toolbar -> Tools, Board and chose the Arduino Uno.
Next step is to define the port. Tools - Port - COM3 (this is where my Arduino is connected to my computer).

Then press the Upload button (the orange arrow), save your file and then Arduino starts compiling the code to the Arduino Uno.
In the buttom there is a status bar that tells you what is going on and if the compiling succeded og failed.

When the Arduino Uno is programmed, I placed a wire (blue) from pin 2 to pin 3 as these are the pins mentioned in the code.
Next up is connecting the oscilloscope to the Arduino Uno. The probe wires I placed on ground and the through hole on pin 2.
The waveforms looks like this:





The groupmembers are:
antti.rytilahti http://fabacademy.org/2021/labs/oulu/students/antti-rytilahti/about/
Kenichi Komatsu  http://fabacademy.org/2021/labs/oulu/students/kenichi-komatsu/
Mona http://fabacademy.org/2021/labs/oulu/students/motahareh-peyvasteh/Home.html
Helle

Individual assignment:
Read the datasheet for the microcontroller you are programming
Program the board you have made to do something, with as many different programming languages and programming environments as possible.