# Home

## Welcome to my new Fab Academy site!
Here you can follow my progress during the [Fab Academy](https://fabacademy.org/) 2021.

I'm very excited and a bit nervous to start this experience. I don´t have much experience in many of the tasks that awaits me. I have a great experience in vector drawing and a fair amount of experience in lasercutting, 3D printing and vinyl printing/cutting but thats just a very small part of this course.

If you want to learn more about me, take a look at the About page. All the weekly assignments that I work on will be documentet in 'Assignments'. Maybe we can learn together. Curious about my final project? Me too! Follow the progress under 'Final project'.

Wish me luck!

Helle
